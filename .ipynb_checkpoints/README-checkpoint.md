# drake_notebooks

Early implementations of basic nonlinear dynamic systems using the Drake 
toolbox. To use you'll need to install download the drake binaries 
(http://drake.mit.edu). As well as the code/notes here 
(https://github.com/RussTedrake/underactuated). Both will need to be added to 
your python path, it should look something like

export PYTHONPATH=path_to_drake/lib/python2.7/site-packages:path_to_underactuated/underactuated/src/:${PYTHONPATH}