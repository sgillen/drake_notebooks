{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# One-Legged Hopper\n",
    "\n",
    "In this set, we're going to spend some time with the [one-legged hopper](http://www.ai.mit.edu/projects/leglab/robots/3D_hopper/3D_hopper.html). This system (which is discussed in great detail in [this paper](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6313238) -- you can reference that paper for some parts of this set!) enables extremely dynamic walking behavior that bounces between *stance phases* when the foot is on the ground, and *flight phases* when the foot is in the air. The system is dramatically underactuated in both phases, but as you'll see in this problem set, it can still be stabilized!\n",
    "\n",
    "## Dynamics\n",
    "The (planar) one-legged hopper consists of a body with mass $m_b$ and a foot with a mass $m_f$, with the connection between the foot and the body being a single (actuated, torque-controlled) pin joint, and the leg being springy with controllable spring constant.\n",
    "\n",
    "<p align=\"center\">\n",
    "  <img src=\"./planar_hopper.png\" width=\"350\"/>\n",
    "</p>\n",
    "\n",
    "The planar one-legged hopper, diagramed above, has state\n",
    "\n",
    "$$ \\mathbb{x} = \\left[ \\begin{array} \\\\ q \\\\ \\dot q \\end{array}\\right] \\ \\ \\ q = \\left[\\begin{array}\\\\ x \\\\ z \\\\ \\theta \\\\ \\alpha \\\\ l\\end{array}\\right] $$\n",
    "\n",
    "for floating base coordinates $x, z, \\theta$, hip angle $\\alpha$, and leg extension $l$ limited to $\\left[-\\infty, l_{max}\\right]$. This joint limit is implemented with a highly damped one-sided (i.e., only active when the limit is exceeded) spring. The \"springiness\" in the leg is represented by a force $f_{spring} = K_l * (l_{rest} - l)$ that pushes the foot back to full extension when it is compressed. **The system has two control inputs: instantaneous control of $l_{rest}$, and direct torque control of the leg angle.**\n",
    "\n",
    "This system is hybrid due to the joint limit and ground contact, and (usually) oscillates between two contact modes:\n",
    "\n",
    "1) **Flight**: When the foot is not in contact with the ground and the leg is fulled extended to $l = l_{max}$ (these usually occur simultaneously, as in flight there's nothing stopping the leg from passively extending). In this mode, the whole assembly flies through the air under the influence of gravity.\n",
    "\n",
    "2) **Stance**: When the foot is in contact with the ground, a ground reaction force (also represented with a highly damped one-sided spring) pushes the foot out of collision with the ground.\n",
    "\n",
    "## Controlling the Hopper\n",
    "\n",
    "As discussed in lecture, one might think about controlling this system by separating it into three separate control problems:\n",
    "\n",
    "1) Controlling the hopping height of the body by pushing off the ground while in stance phase\n",
    "\n",
    "2) Controlling the horizontal velocity of the body by choosing where to place the foot during the next stance phase (which requires exerting torque during flight phase to aim the leg)\n",
    "\n",
    "3) Controlling the angular velocity of the body by applying a torque to the leg while in stance phase\n",
    "\n",
    "In this section we'll play with a planar model of the hopper. **We've supplied a controller that will take care of regulating the hopping height (using Raibert's very simple controller) by modifying the spring rest length.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## What you have to do\n",
    "\n",
    "Peruse the provided planar hopper controller class in *hopper_2d.py* and understand what it is currently accomplishing. The *Hopper2dController* system implements a controller for the planar 2d hopper, and the *Simulate2dHopper* function loads in the 2d hopper from a robot description file, hooks up the controller, and runs a simulation. The controller calculates its output in *_DoCalcVectorOutput*, but dispatches its decision making to two other methods: \n",
    "\n",
    "- *ChooseSpringRestLength* picks the instantaneous rest length of the spring. We've written this for you, implementing something like Raibert's original hopper height controller.\n",
    "- *ChooseThighTorque* picks a leg angle torque (which directly controls $\\ddot \\alpha$). You have to write this one!\n",
    "\n",
    "**Fill in ChooseThighTorque with a leg angle torque controller that lateral velocity to the desired lateral velocity, and also keeps the body steady ($\\theta = 0$).** Comment your code thoroughly explaining what your controller is doing -- we reserve the right to dock points for functional but seemingly arbitrary code! The code snippets below are here to help you test your system (feel free to change the initial condition, the simulation duration, the lateral velocity target, the xlim and ylim of the visualizer, etc...). As usual, a battery of tests will be used to help validate your system -- feel free to peruse (but don't modify) *test_set_4.py* to see the conditions we're testing.\n",
    "\n",
    "### Advice:\n",
    "- While you're very welcome to explore (any method that passes the tests is technically OK!), I recommend implementing one of the three controllers described in Section IV of [this paper](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6313238&tag=1). In particular, the 3rd controller (\"Servo Attitude\") we found to be particularly effective and simple to implement. Like many \"intuitive\"-style controllers, it has a handful of parameters that must be estimated (e.g. the typical duration of flight and stance phases), which you are free to infer from looking at simulation runs and hard-coding.\n",
    "- Gain tuning will undoubtedly be a stumbling block here. Start as simple as you can: for example, focus first on controlling lateral velocity to 0 (i.e. just maintain balance), then controlling lateral velocity to reasonable speeds, and then finally controlling the body angle to near zero. Only try to tune one number at a time!\n",
    "- Friction with the ground is *not infinite*, so if you see the foot slipping, fix your foot placement or limit your leg angle torques."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The autoreload extension is already loaded. To reload it, use:\n",
      "  %reload_ext autoreload\n"
     ]
    },
    {
     "ename": "SystemExit",
     "evalue": "Failure at bazel-out/k8-opt/bin/multibody/_virtual_includes/kinematics_cache/drake/multibody/kinematics_cache-inl.h:82 in initialize(): condition 'q.rows() == q_in.rows()' failed.",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mSystemExit\u001b[0m                                Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-1-49ff94b55635>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     17\u001b[0m                                \u001b[0mduration\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;36m10\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     18\u001b[0m                                \u001b[0mdesired_lateral_velocity\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;36m0\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 19\u001b[0;31m                                print_period = 1.0)\n\u001b[0m\u001b[1;32m     20\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     21\u001b[0m \u001b[0;32mprint\u001b[0m \u001b[0;34m\"Done!\"\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/home/sgillen/notebooks/underactuated/set_4/hopper_2d.pyc\u001b[0m in \u001b[0;36mSimulate2dHopper\u001b[0;34m(x0, duration, desired_lateral_velocity, print_period)\u001b[0m\n\u001b[1;32m    251\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    252\u001b[0m     \u001b[0;31m# Simulate!\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 253\u001b[0;31m     \u001b[0msimulator\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mStepTo\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mduration\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    254\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    255\u001b[0m     \u001b[0;32mreturn\u001b[0m \u001b[0mtree\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mcontroller\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mstate_log\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/home/sgillen/notebooks/underactuated/set_4/hopper_2d.pyc\u001b[0m in \u001b[0;36m_DoCalcVectorOutput\u001b[0;34m(self, context, u, x, y)\u001b[0m\n\u001b[1;32m    149\u001b[0m         \u001b[0;31m# here is the state of the hopper.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    150\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 151\u001b[0;31m         \u001b[0ml_rest\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mChooseSpringRestLength\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mu\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    152\u001b[0m         \u001b[0;31m# Apply a force on the leg extension prismatic joint\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    153\u001b[0m         \u001b[0;31m# that simulates the passive spring force (given the\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/home/sgillen/notebooks/underactuated/set_4/hopper_2d.pyc\u001b[0m in \u001b[0;36mChooseSpringRestLength\u001b[0;34m(self, X)\u001b[0m\n\u001b[1;32m     68\u001b[0m         \u001b[0;31m# Run out the forward kinematics of the robot\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     69\u001b[0m         \u001b[0;31m# to figure out where the foot is in world frame.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 70\u001b[0;31m         \u001b[0mkinsol\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mhopper\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mdoKinematics\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     71\u001b[0m         \u001b[0mfoot_point\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mnp\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0marray\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m0.0\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m0.0\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m-\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mhopper_leg_length\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     72\u001b[0m         foot_point_in_world = self.hopper.transformPoints(kinsol, \n",
      "\u001b[0;31mSystemExit\u001b[0m: Failure at bazel-out/k8-opt/bin/multibody/_virtual_includes/kinematics_cache/drake/multibody/kinematics_cache-inl.h:82 in initialize(): condition 'q.rows() == q_in.rows()' failed."
     ]
    },
    {
     "ename": "SystemExit",
     "evalue": "Failure at bazel-out/k8-opt/bin/multibody/_virtual_includes/kinematics_cache/drake/multibody/kinematics_cache-inl.h:82 in initialize(): condition 'q.rows() == q_in.rows()' failed.",
     "output_type": "error",
     "traceback": [
      "An exception has occurred, use %tb to see the full traceback.\n",
      "\u001b[0;31mSystemExit\u001b[0m\u001b[0;31m:\u001b[0m Failure at bazel-out/k8-opt/bin/multibody/_virtual_includes/kinematics_cache/drake/multibody/kinematics_cache-inl.h:82 in initialize(): condition 'q.rows() == q_in.rows()' failed.\n"
     ]
    }
   ],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "%tb\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import HTML\n",
    "from underactuated import PlanarRigidBodyVisualizer\n",
    "\n",
    "import hopper_2d\n",
    "\n",
    "x0 = np.zeros(10)\n",
    "x0[1] = 2\n",
    "x0[4] = 0.5\n",
    "\n",
    "# Run the simulation\n",
    "hopper, controller, state_log = \\\n",
    "    hopper_2d.Simulate2dHopper(x0 = x0,\n",
    "                               duration=10,\n",
    "                               desired_lateral_velocity = 0,\n",
    "                               print_period = 1.0)\n",
    "\n",
    "print \"Done!\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'hopper' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-4-329417f36152>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0;31m# Visualize the simulation as a video\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 2\u001b[0;31m \u001b[0mviz\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mPlanarRigidBodyVisualizer\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mhopper\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mxlim\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m-\u001b[0m\u001b[0;36m2\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m2\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mylim\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m-\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m4\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      3\u001b[0m \u001b[0mviz\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfig\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mset_size_inches\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m10\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m5\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      4\u001b[0m \u001b[0mani\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mviz\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0manimate\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mstate_log\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m30\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mrepeat\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mTrue\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      5\u001b[0m \u001b[0mplt\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mclose\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mviz\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfig\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mNameError\u001b[0m: name 'hopper' is not defined"
     ]
    }
   ],
   "source": [
    "# Visualize the simulation as a video\n",
    "viz = PlanarRigidBodyVisualizer(hopper, xlim=[-2, 2], ylim=[-1, 4])\n",
    "viz.fig.set_size_inches(10, 5)\n",
    "ani = viz.animate(state_log, 30, repeat=True)\n",
    "plt.close(viz.fig)\n",
    "HTML(ani.to_html5_video()) # This needs to be the last line for the video to display"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'state_log' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-5-d65a95b75be2>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0;31m# Plot traces of certain states\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      2\u001b[0m \u001b[0mplt\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfigure\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mset_size_inches\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m10\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m5\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 3\u001b[0;31m \u001b[0mplt\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mplot\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mstate_log\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0msample_times\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mstate_log\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mdata\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m0\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m:\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      4\u001b[0m \u001b[0mplt\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mplot\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mstate_log\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0msample_times\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mstate_log\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mdata\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m0\u001b[0m\u001b[0;34m+\u001b[0m\u001b[0;36m5\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m:\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      5\u001b[0m \u001b[0mplt\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mgrid\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mTrue\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mNameError\u001b[0m: name 'state_log' is not defined"
     ]
    },
    {
     "data": {
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7fea04747290>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Plot traces of certain states\n",
    "plt.figure().set_size_inches(10, 5)\n",
    "plt.plot(state_log.sample_times(), state_log.data()[0, :])\n",
    "plt.plot(state_log.sample_times(), state_log.data()[0+5, :])\n",
    "plt.grid(True)\n",
    "plt.legend([\"body_x\", \"body_x_d\"])\n",
    "\n",
    "plt.figure().set_size_inches(10, 5)\n",
    "plt.plot(state_log.sample_times(), state_log.data()[1, :])\n",
    "plt.plot(state_log.sample_times(), state_log.data()[1+5, :])\n",
    "plt.grid(True)\n",
    "plt.legend([\"body_z\", \"body_z_d\"])\n",
    "\n",
    "plt.figure().set_size_inches(10, 5)\n",
    "plt.plot(state_log.sample_times(), state_log.data()[2, :])\n",
    "plt.plot(state_log.sample_times(), state_log.data()[2+5, :])\n",
    "plt.legend([\"body_theta\", \"body_theta_d\"])\n",
    "plt.grid(True)\n",
    "\n",
    "plt.figure().set_size_inches(10, 5)\n",
    "plt.plot(state_log.sample_times(), state_log.data()[4, :])\n",
    "plt.plot(state_log.sample_times(), state_log.data()[4+5, :])\n",
    "plt.legend([\"leg_extension\", \"leg_extension_d\"])\n",
    "plt.xlim([0.0, 1.0])\n",
    "plt.grid(True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run tests"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "# Run the tests\n",
    "os.popen(\"python test_set_4.py test_results.json\")\n",
    "\n",
    "# Print the results json for review\n",
    "import test_set_4\n",
    "print test_set_4.pretty_format_json_results(\"test_results.json\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
